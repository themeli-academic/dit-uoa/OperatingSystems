
#include "settings.h"


// ENC1 or 2
char processName[10];

int main(int argc, char ** argv)

{
    semaphore dataLeft,dataRight,lockLeft,lockRight;
    memory memLeft,memRight, directionMem;


    semaphore * Left, *Right;
    Left = (semaphore*) malloc(sizeof(semaphore));
    Right = (semaphore*) malloc(sizeof(semaphore));

    semaphore * OtherEncReaderDataSem = (semaphore*)malloc(sizeof(semaphore));
    directionMem.key = dirMem;
    // ENC should always have at least 3 arguments
    // the shared memory to join to, and the name of the process to spawn
    if(argc < 1)
    {
        printf("Error : Expected 1 arguments for ENC, got %d:\n",argc);
        int i=0;
        for(;i<argc;++i) printf("\t%s\n",argv[i]);
        return FAILURE;
    }
    // figure out which process we are. Last argument tells us.
    // if it is "c" -> spawn a c-process (chan) -> we are enc1
    // if it is "p" -> spawn a p-process (chan) -> we are enc2
    if(strcmp(argv[argc-1],"c")==0)
    {
        sprintf(processName,"ENC1");
        memLeft.key = mP1E1;
        lockLeft.key = P1E1;
        dataLeft.key = dP1E1;

        memRight.key = mE1C;
        lockRight.key = E1C;
        dataRight.key = dE1C;

        Left->key = E1L;
        Right->key = E1R;

        OtherEncReaderDataSem->key = dE2P2;
        OtherEncReaderDataSem->id = -1;

    }
    else if(strcmp(argv[argc-1],"p")==0)
    {
        sprintf(processName,"ENC2");
        memLeft.key = mCE2;
        lockLeft.key = CE2;
        dataLeft.key = dCE2;

        memRight.key = mE2P2;
        lockRight.key = E2P2;
        dataRight.key = dE2P2;
        Left->key = E2L;
        Right->key = E2R;

        OtherEncReaderDataSem->key = dP1E1;
        OtherEncReaderDataSem->id = -1;
    }
    else
    {
        printf("ENC: Undefined last ENC argument: [%s]\n",argv[argc-1]);
        printf("c or p was expected.Exiting.\n");
        return FAILURE;
    }


    PRINT("Connecting to left shared memory");
    //  connect to shared memory
    CHECK(connectToSharedMemory (&memLeft),"Connect to left shared memory");
    //  connect to direction shared memory
    CHECK(connectToDirectionMemory (&directionMem),"Connect to direction shared memory");

    // create and connect to semaphore
    // connect
    PRINT("Connecting to left lock and data");
    CHECK(connectToSemaphore(&lockLeft),"Connect to left lock");
    CHECK(connectToSemaphore(&dataLeft),"Connect to left data");
    //create
    PRINT("Creating right lock and data");
    CHECK(initSemaphore(&lockRight,1),"Created to right lock");
    CHECK(initSemaphore(&dataRight,0),"Created to right data");

    // also create direction semaphores
    // the mallocs are up, with the declarations, because we figure out which ENC we are
    // above. So we'll set the semaphore keys there
    PRINT("Creating direction sem");
    CHECK(initSemaphore(Left,0),"Init left direction semaphore.");
    CHECK(initSemaphore(Right,1),"Init right direction semaphore.");

    // create the right shared memory
    PRINT("Creating right memory");
    CHECK(createSharedMemory(&memRight),"Created right shared memory");



    // fork & exec
    pid_t pid = fork();
    CHECK(pid,"Fork failed.");

    // if we're in ENC1, exec chan, else exec P2
    if(strcmp(processName,"ENC1") == 0)
    {
        if(pid == 0)
        {
               // child process
//                printf("%s is spawing CHAN\n",processName);
                if(ENC_CHAN_SEPARATE_TERMINALS == 0)
                {
                    execl("c","",NULL);
                }
                else
                {
                    // spawn in diff term for debuggin
                    char command[100];
                    // dummy arg exists so that arg. count of P2 is > 1
                    // check p.c outer if: if argc==1, it's P1. Dumb ..
                    sprintf(command,"x-terminal-emulator -e \"./c  &\"");
                    int returnValue = system(command);
                    if(returnValue < 0)
                    {
                        printf("Something went wrong while calling command:\n%s\n.Exiting.",command);
                        return FAILURE;
                    }
                    return SUCCESS;
                }
        }

        else
        {
            // parent
            if(ENC_CHAN_SEPARATE_TERMINALS == 1)
            {
                if (waitpid(pid, NULL, 0) < 0)
                {
                    perror("Failed to collect child process");
                    return FAILURE;
                }
                else
                {
                    printf("Consumed C-creating child process successfully.\n");
                }
            }
        }

    }
    else
    {
        if(pid == 0)
        {
               // child process
                printf("%s is spawing P2\n",processName);
                // to have a terminal UI, we'll spawn the process in a terminal
                //execl("p",keyString, NULL);
                char command[100];
                // dummy arg exists so that arg. count of P2 is > 1
                // check p.c outer if: if argc==1, it's P1. Dumb ..
                sprintf(command,"x-terminal-emulator -e \"./p dummyArg &\"");
                int returnValue = system(command);
                if(returnValue < 0)
                {
                    printf("Something went wrong while calling command:\n%s\n.Exiting.",command);
                    return FAILURE;
                }
                return SUCCESS;
        }
        else
        {
            // parent
            if (waitpid(pid, NULL, 0) < 0)
            {
                perror("Failed to collect child process");
                return FAILURE;
            }
            else
            {
                printf("Consumed P2-creating child process successfully.\n");
            }
        }


    }


    // end of creation
    // start of behaviour

    // initial source, dest ptrs

    pthread_t thread1,thread2;


    if(strcmp(processName,"ENC1") == 0)
    {
        // ENC1
        // ----
        // encode to the right
        char msgEL[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        char msgCR[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];

        continuousArgs CompositeArgs1transfer;
        threadArgs read1transfer = makeThreadArgs(processName,msgEL,&memLeft,&lockLeft,&dataLeft,Right,&directionMem);
        threadArgs write1transfer = makeThreadArgs(processName,msgEL,&memRight,&lockRight,&dataRight,Right,&directionMem);
        read1transfer.DesiredDirection = RIGHT;
        write1transfer.DesiredDirection = RIGHT;
        CompositeArgs1transfer.read = &read1transfer;
        CompositeArgs1transfer.write = &write1transfer;

        CHECK(pthread_create(&thread1,NULL,(void*)&ENCencoder,&CompositeArgs1transfer),"Failed to create enc1 transfer");

        // check to the left
        continuousArgs CompositeArgs1Check;
        threadArgs read1Check = makeThreadArgs(processName,msgCR,&memRight,&lockRight,&dataRight,Left,&directionMem);
        threadArgs write1Check = makeThreadArgs(processName,msgCR,&memLeft,&lockLeft,&dataLeft,Left,&directionMem);
        read1Check.DesiredDirection = LEFT;
        write1Check.DesiredDirection = LEFT;
        CompositeArgs1Check.read = &read1Check;
        CompositeArgs1Check.write = &write1Check;
        read1Check.resendSem = OtherEncReaderDataSem;
        CHECK(pthread_create(&thread2,NULL,(void*)&ENCdecoder,&CompositeArgs1Check),"Failed to create enc2 check input thead");

    }
    else
    {
        // ENC2
        // -----
        char msgER[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        char msgCL[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        // encode to the left
        continuousArgs CompositeArgs2transfer;
        threadArgs read2transfer = makeThreadArgs(processName,msgER,&memRight,&lockRight,&dataRight,Left,&directionMem);
        threadArgs write2transfer = makeThreadArgs(processName,msgER,&memLeft,&lockLeft,&dataLeft,Left,&directionMem);
        read2transfer.DesiredDirection = LEFT;
        write2transfer.DesiredDirection = LEFT;
        CompositeArgs2transfer.read = &read2transfer;
        CompositeArgs2transfer.write = &write2transfer;

        CHECK(pthread_create(&thread1,NULL,(void*)&ENCencoder,&CompositeArgs2transfer),"Failed to create enc2 transfer");


        // check to the right
        continuousArgs CompositeArgs2Check;
        threadArgs read2Check = makeThreadArgs(processName,msgCL,&memLeft,&lockLeft,&dataLeft,Right,&directionMem);
        threadArgs write2Check = makeThreadArgs(processName,msgCL,&memRight,&lockRight,&dataRight,Right,&directionMem);
        read2Check.DesiredDirection = RIGHT;
        write2Check.DesiredDirection = RIGHT;
        CompositeArgs2Check.read = &read2Check;
        CompositeArgs2Check.write = &write2Check;
        read2Check.resendSem = OtherEncReaderDataSem;

        CHECK(pthread_create(&thread2,NULL,(void*)&ENCdecoder,&CompositeArgs2Check),"Failed to create enc2 check input thead");

    }



    // end of behaaviour
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);




    semctl (Left->id,0,IPC_RMID,0);
    semctl (Right->id,0,IPC_RMID,0);
    free(Left); free(Right); free(OtherEncReaderDataSem);
    if(PAUSE_BEFORE_EXIT) delayExit(processName);
    return SUCCESS;
}

