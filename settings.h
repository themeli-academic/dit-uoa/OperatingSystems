// definitions

#define MAX_MESSAGE_SIZE 50
#define MAX_ENCODING_SIZE 100
#define ALLOCATE (char*)malloc(MAX_ENCODING_SIZE + MAX_MESSAGE_SIZE * sizeof(char))
#define PROB_CORRUPTION   50 // value from 0 to 100
#define MESSAGE_CHSUM_SEPARATOR '#'
#define SUCCESS 1
#define FAILURE -1
#define PRINT(what) printf("%s: %s\n",processName,what);
#define CHECK(value,what) if(value<0){ \
    printf("*******: process %s : %s failed. Exiting...\n",processName,what);\
    return FAILURE;}
#define ENC_CHAN_SEPARATE_TERMINALS 1
#define PAUSE_BEFORE_EXIT 0
#define VERBOSITY 1

// struct to store process info
enum DIRECTION {LEFT=0, RIGHT, NONE};
typedef enum DIRECTION DIRECTION;


// generic library includes
#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <errno.h>

// MD5 and
#include "md5.h"

// threading
#include <pthread.h>

// shared mem
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/ipc.h>
enum SHMKEYS {mP1E1=1, mE1C, mCE2, mE2P2, dirMem};
typedef struct
{
    int id;
    int key;
    char * ptr;
} memory;

// semaphores
#include <sys/sem.h>

enum SEMKEYS {P1E1=100, E1C, CE2, E2P2,
              dP1E1, dE1C, dCE2, dE2P2,
              // right - dir sems 108 - 112
             P1R,E1R,CR,E2R,P2R,
              // left dir sems 113 - 117
             P1L ,E1L,  CL, E2L, P2L};
// semaphore pair for a shared memory
// contains key and id of a semaphore
typedef struct
{
    int id;
    int key;
} semaphore;
// define semun here
union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};

char * errorstr()
{
    return strerror(errno);
}
void printError()
{
    printf("Error is : %s\n",errorstr());
}

// down & up semaphore functions
void lock(semaphore * sem)
{
      struct sembuf oper[1] = {0,-1,0};
      semop(sem->id, &oper[0], 1);
}
void unlock(semaphore * sem)
{
      struct sembuf oper[1] = {0,1,0};
      semop(sem->id, &oper[0], 1);
}

// creates and initializes a semaphore
int initSemaphore(semaphore * sem,int startValue)
{
    union semun arg;
    sem->id = semget (sem->key, 1, 0666|IPC_CREAT);
    if(sem->id < 0)
    {
        printf("\tinitSemaphore: Failed to semaphore with key %d , id %d \n",sem->key, sem->id);
        printError();
        return FAILURE;
    }
    arg.val=startValue;
    semctl (sem->id,0,SETVAL,arg);
    if(VERBOSITY)
    printf("\tCreated semaphore with key %d , id %d , initialized to %d\n",sem->key, sem->id,startValue);
    return SUCCESS;
}
// connect to semaphore
int connectToSemaphore(semaphore * sem)
{
    sem->id = semget (sem->key, 1, 0666); // no create
    if(sem->id < 0)
    {
        printf("connectToSemaphore: Failed to get existing semaphore with key %d.\n",sem->key);
        printError();
        return FAILURE;
    }
    if(VERBOSITY)
        printf("\tConnected to semaphore with key %d , id %d \n",sem->key, sem->id);

    return SUCCESS;
}


// -------------------




// an initialization function, to run only once
void initRandomize()
{
	srand((unsigned)time(NULL));

}

// random real numbers
double getRandomReal()
{
	return ((double)rand()/(double)RAND_MAX);

}
// random integer numbers in a range
int getRandomInteger(int low,int high)
{
	return (rand() % (high - low + 1)) + low;
	
}
char getRandomAlphanumeric()
{
	// ASCII 32 to 126 
	return (char) getRandomInteger(32,126);
}

// connect to sh mem
int connectToSharedMemory(memory * mem)
{
	
        mem->id = shmget(mem->key, MAX_MESSAGE_SIZE, 0666);

        if(mem->id < 0)
	{
                printf("connectToSharedMemory : Shmget failed.\n");	return FAILURE;
	}
        mem->ptr = (char*) shmat(mem->id,0,0);
        if(mem->ptr == (void*) -1)
	{
                printf("connectToSharedMemory : Shmat failed to connect to key %d.\n",mem->key); return FAILURE;
	}
        if(VERBOSITY)
        printf("\tConnected to shared memory key %d - id %d.\n",mem->key,mem->id);
	return SUCCESS;
}
// create sh mem

int createSharedMemory(memory * mem)
{
        mem->id = shmget(mem->key, MAX_MESSAGE_SIZE, 0666 | IPC_CREAT);
        //printf("Create shared memory with %d\n",key);
        if(mem->id < 0)
        {
		printf("Shmget failed.\n");	return FAILURE;
	}
        //printf("Created shared memory with id : %d and key : %d\n",*sm_id,key);
        mem->ptr = (char*) shmat(mem->id,0,0);
        if(mem->ptr == (void*) -1)
        {
                printf("createSharedMemory : Shmat failed . key was %d .\n",mem->key);
                return FAILURE;
        }
        if(VERBOSITY)
            printf("\tCreated shared memory with key %d - id %d.\n",mem->key,mem->id);

	return SUCCESS;
}
// create direction sh mem

int createDirectionMemory(memory * mem)
{
        mem->id = shmget(mem->key, 2, 0666 | IPC_CREAT);
        //printf("Create shared memory with %d\n",key);
        if(mem->id < 0)
        {
                printf("Shmget failed.\n");	return FAILURE;
        }
        //printf("Created shared memory with id : %d and key : %d\n",*sm_id,key);
        mem->ptr = (char*) shmat(mem->id,0,0);
        if(mem->ptr == (void*) -1)
        {
                printf("createDirectionMemory : Shmat failed . key was %d .\n",mem->key);
                return FAILURE;
        }

        if(VERBOSITY)
            printf("\tCreated direction memory with key %d - id %d.\n",mem->key,mem->id);

        return SUCCESS;
}
// connect to direction sh mem
int connectToDirectionMemory(memory * mem)
{

        mem->id = shmget(mem->key, 2, 0666);

        if(mem->id < 0)
        {
                printf("connectToSharedMemory : Shmget failed.\n");	return FAILURE;
        }
        mem->ptr = (char*) shmat(mem->id,0,0);
        if(mem->ptr == (void*) -1)
        {
                printf("connectToSharedMemory : Shmat failed to connect to key %d.\n",mem->key); return FAILURE;
        }

        if(VERBOSITY)
            printf("\tConnected to direction memory key %d - id %d.\n",mem->key,mem->id);
        return SUCCESS;
}
// not a star wars ability
void forcePrint()
{
    fflush(stdout); fflush(stderr);
}


// MD5 checksum function
char * getMD5(const char * message) {

    char * chsum = (char*)malloc(MAX_ENCODING_SIZE);
    unsigned  * d = md5(message, (int)strlen(message));
    WBunion u;
    int j,k;
    //printf("checksum of %s is : ",message);

    //printf("= 0x");
    for (j=0;j<4; j++){
        u.w = d[j];
        for (k=0;k<4;k++)
        {
            sprintf(&chsum[j*8 + 2*k],"%02x",u.b[k]);
            //printf("%02x",u.b[k]);
        }
    }
    //printf("\n");

    if(VERBOSITY)
    printf("str checksum of %s is : %s\n",message,chsum);
    return chsum;
}



// show active semaphores
void showSemaphores()
{
    system("ipcs -s");
}

// RW function
void * readwrite(char * src, char * dst)
{
    strcpy(dst,src);
}
// struct to change direction relatively painlessly
typedef struct directionSemaphores
{
    //semaphore * p1l,* p1r,*e1l,*e1r,*cl,*cr,*e2l,*e2r,*p2l,*p2r;
    semaphore * right[5];
    semaphore * left[5];
    semaphore * data[4];
}AllDirectionSemaphores;
// struct for thread arguments
typedef struct argsStruct
{
    char * whoAmI;
    char * data;
    memory * mem,*dirMem;
    semaphore * semlock, *semdata, *direction,*resendSem;
    DIRECTION DesiredDirection;
    //AllDirectionSemaphores * AllDirSems;
}threadArgs;

// make thread argument struct
threadArgs makeThreadArgs(char * who,char * data,memory * mem,semaphore * semlock, semaphore * semdata,semaphore * direction,memory * dirMem)
{
    threadArgs args;
    args.dirMem = dirMem;
    args.whoAmI = who;
    args.mem = mem;
    args.data = data;   // temporary storage
    args.semlock = semlock;
    args.semdata = semdata;
    args.direction = direction;
    args.resendSem = NULL;
    //args.AllDirSems = NULL;

    return args;
}

// gets the current direction value from the direction shared memory byte
DIRECTION currDirection(threadArgs * args)
{
    if((args->dirMem->ptr) == NULL)
    {
        printf("!!!! null direction mem in argsDirection() !! \n");
        return -1;
    }
    if((strcmp(args->dirMem->ptr,"R") == 0) || (strcmp(args->dirMem->ptr,"RR") == 0)) return RIGHT;
    else if((strcmp(args->dirMem->ptr,"L") == 0) || (strcmp(args->dirMem->ptr,"LL") == 0)) return LEFT;
    else
    {
        printf("!!!!undefined mem data :[%s] in argsDirection() !! \n",(args->dirMem->ptr));
        return -1;
    }
}
// switches the value from the direction shared memory byte
void switchDirection(threadArgs * args)
{

    if(VERBOSITY)
        printf("Switched dir-mem value from %s to ",(args->dirMem->ptr));
    if((args->dirMem) == NULL)
    {
        printf("!!!! null direction mem in switchDirection() !! \n");
        return ;
    }
    if(currDirection(args) == RIGHT)
    {
        args->dirMem->ptr[0] = 'L';
        args->dirMem->ptr[1] = '\0';

    }
    else if(currDirection(args) == LEFT)
    {
        args->dirMem->ptr[0] = 'R';
        args->dirMem->ptr[1] = '\0';

    }
    else
    {
        printf("!!!!undefined mem data :[%s] in switchDirection() !! \n",(args->dirMem->ptr));
        return ;

    }

    if(VERBOSITY)
        printf("[%s] \n",(args->dirMem->ptr));

}

// unlock data semaphores of the shared memories
void unlockAllData(AllDirectionSemaphores * sems)
{
    int i=0;
    //printf("About to unlock data : \n");
    for(i=0;i<4;++i)
    {
        unlock(sems->data[i]);
        if(VERBOSITY)
            printf("Unlocked sem-key %d\n",sems->data[i]->key);
    }
    //printf("Paused.\n");forcePrint();pause();

}
// unlock the semaphores of the specified direction
void toggleDirectionToAllSemaphores(AllDirectionSemaphores * sems,DIRECTION dir)
{
    int i=0;
    //

    if(dir == RIGHT)
    {

        if(VERBOSITY)
            printf("Unlocking RIGHT direction semaphores.\n");
        for(i=0;i<5;++i)
        {
           //printf("Setting RIGHT dir : lockin left, unlockin right #%d , id %d\n",i,sems->left[i]->id);
            //printf("\tright/left id : %d , %d \n",sems->left[i]->id,sems->right[i]->id);
            //lock(sems->left[i]);
            //printf("DONE lockin left, unlockin right #%d , id %d\n",i,sems->left[i]->id);
            //printf("Unlockin right #%d ,key : %d  id %d\n",i,sems->left[i]->key,sems->left[i]->id);
            unlock(sems->right[i]);

        }
    }
    else if (dir == LEFT)
    {

        if(VERBOSITY)
            printf("Unlocking LEFT direction semaphores.\n");
        // enable right direction : lock right, unlock left
        for( i=0;i<5;++i)
        {
           // printf("Setting LEFT dir : lockin right #%d ,key : %d  id %d\n",i,sems->right[i]->key,sems->right[i]->id);
            //lock(sems->right[i]);
            //printf("Unlockin left #%d ,key : %d  id %d\n",i,sems->left[i]->key,sems->left[i]->id);
            unlock(sems->left[i]);
        }
    }
    // direction has been toggled.
    // now unlock all the data semaphores, to wake up the threads reading in the opposite direction
    // (blocked on data) , so that they will get blocked on direction.


    if(VERBOSITY)
        printf("Toggled direction\n");

}
// get all semaphores for direction change
AllDirectionSemaphores *getDirectionSemaphores()
{
    AllDirectionSemaphores  * sems =(AllDirectionSemaphores*)malloc(sizeof(AllDirectionSemaphores));
    int i = 0;
    for(;i<5;++i)
    {
        sems->right[i] = (semaphore*) malloc(sizeof(semaphore));
        sems->left[i] = (semaphore*) malloc(sizeof(semaphore));
        if(i<4)
        {
            sems->data[i] = (semaphore*) malloc(sizeof(semaphore));
            sems->data[i]->key = 104 + i;
            if(0> connectToSemaphore(sems->data[i])) { printf("getDirectionSemaphores : Failed to connect to data dir.semaphore # %d\n",i); return NULL; }
        }
        sems->right[i]->key = 108 + i;
        sems->left[i]->key = 113 + i;
        if(0> connectToSemaphore(sems->right[i])) { printf("getDirectionSemaphores : Failed to connect to right dir.semaphore # %d\n",i); return NULL; }
        if(0> connectToSemaphore(sems->left[i])) { printf("getDirectionSemaphores : Failed to connect to left dir.semaphore # %d\n",i); return NULL; }

    }
    // starting #define of direction semaphores is at 108
    //printf("Fetched all semaphores\n");
    return sems;
}
void deleteDirectionSemaphores(AllDirectionSemaphores * sems)
{
    int i = 0;
    for(;i<5;++i)
    {
        if(i<4)
        {
            free( sems->data[i] );
        }
        free(sems->left[i]);
        free(sems->right[i]);
    }
}

// clean memory
void emptyMem(char * m)
{
    int i=0;
    for(;i<MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1;++i) m[i] = '\0';
}

// function to write to shared memory
void writeToMemory(threadArgs * args)
{

    if(VERBOSITY)
        printf("\t~~~%s dir-%d Writing to shared memory with key %d\n",args->whoAmI,args->DesiredDirection,args->mem->key);

    lock(args->semlock);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d WSM to mem key %d passed regular lock key : %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semlock->key);
    readwrite(args->data,args->mem->ptr);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d WSM mem key %d :  wrote [%s]\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->data);
    unlock(args->semdata);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d WSM mem key %d : up'd data key : %d \n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semdata->key);
    unlock(args->semlock);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d WSM  mem key %d : unlocked regular, key: %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semlock->key);
}
// function to read from shared memory
int readFromMemory(threadArgs * args)
{
    int didRead=0;
    // do not do anything if there's no data

    if(VERBOSITY)
            printf("\t\t~~%s dir-%d RSM : mem %d , probing direction lock : %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->direction->key);

    lock(args->direction);

    if(VERBOSITY)
            printf("\t\t~~%s dir-%d RSM : mem %d , passed direction lock : %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->direction->key);


    lock(args->semdata);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d RSM : passed data lock mem id %d : down'd data: %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semdata->key);
    // if the process is unlocked but the direction is opposite,
    // release the data lock and return.
    DIRECTION opposite ;
    if(args->DesiredDirection == RIGHT) opposite = LEFT;
    else opposite = RIGHT;
    if(currDirection(args) != args->DesiredDirection)
    //if(currDirection(args) == opposite)
    {
        // no need to unlock data semaphores:
        // they were set to 1 temporarily, to de-stuck the reader threads that were
        // reading in the old direction

        if(VERBOSITY)
                printf("\t\t<---- ~~%s dir-%d RSM: DIRECTION shift %d->%d, reader returning.\n",args->whoAmI,args->DesiredDirection,currDirection(args),args->DesiredDirection);
        return didRead;
    }
    else
    {
        ;//printf("\t\t --- didn't change direction: -- ~~%s dir-%d , curr %d.\n",args->whoAmI,args->DesiredDirection,currDirection(args));

    }


    lock(args->semlock);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d  RSM passed regular lock , mem key %d , lock key : %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semlock->key);
    args->data = (char*)malloc(MAX_MESSAGE_SIZE + MAX_ENCODING_SIZE * sizeof(char));
    readwrite(args->mem->ptr,args->data);
    emptyMem(args->mem->ptr);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d RSM memkey %d :did read : %s , msg was %s\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->data,args->mem->ptr);
    unlock(args->semlock);
    if(VERBOSITY)
        printf("\t\t~~%s dir-%d RSM mem key %d  lock key : %d\n",args->whoAmI,args->DesiredDirection,args->mem->key,args->semlock->key);
    unlock(args->direction);
    didRead = 1;
    return didRead;

}

// a struct to contain a pair of read , write memory & semaphores for
// "bridge threads" (threads that both read and write - basically every thread except P-threads)
typedef struct strct
{
    threadArgs * read;
    threadArgs * write;
}continuousArgs;

// get message
void * getUserInput(char * msg)
{

    printf("Give message: ");
    //scanf("%s",msg); // todo check size;
    fgets(msg,MAX_MESSAGE_SIZE,stdin);
    // drop the last newline, though
    msg[strlen(msg)-1] = '\0';
    if(VERBOSITY)
    printf("Scanned message: [%s].\n",msg);
    return NULL;
}
void Psender(threadArgs * args)
{

    if(VERBOSITY)
        printf("%s : Psender  des/cur %d / %d , \n",args->whoAmI,args->DesiredDirection,currDirection(args));
    while(1)
    {
        getUserInput(args->data);
        if(strlen(args->data) == 0){ printf("Ignoring zero-length input.\n"); continue; }

        // we assume it's pretty improbable that both users will send
        // simultaneously
        // if the P-process gets a message which would reverse direction

        if(args->DesiredDirection != currDirection(args))
        {
            if(VERBOSITY)
            {
                printf("%s, continuousPsend : changing direction to ",args->whoAmI);
                if(args->DesiredDirection == LEFT) printf("LEFT \n");
                else printf("RIGHT\n");
            }
            // change the direction
            switchDirection(args);
            AllDirectionSemaphores * sems = getDirectionSemaphores();
            // unlock all data, to make the ( now reverse ) threads block themselves
            unlockAllData(sems);
            // toggle every direction sem
            toggleDirectionToAllSemaphores(sems,args->DesiredDirection);
            deleteDirectionSemaphores(sems);
            //printf("%s : pPsender new des/cur %d / %d , \n",args->whoAmI,args->DesiredDirection,currDirection(args));

        }

        writeToMemory(args);
        if(strcmp(args->data,"TERM")==0) break;
    }
    if(VERBOSITY)
        printf("%s Psender returning.\n",args->whoAmI);

}
void Preceiver(threadArgs * args)
{

    while(1)
    {
        int status = readFromMemory(args);
        if (status==0) continue;
        printf("\n >>>> %s got data : \"%s\"\n",args->whoAmI,args->data);
        if(strcmp(args->data,"TERM") == 0)
        {

            if(strlen(args->dirMem->ptr) == 1)
            {
                // have to do stuff to kill the threads of the other direction

                // unlock the data from the mem you just read. "TERM" is already there
                //switch direction

                switchDirection(args);
                // set that so that the P-receiver in the other end wont reverse again
                // and propagate the TERM
                args->dirMem->ptr[1] = args->dirMem->ptr[0];
                AllDirectionSemaphores * sems = getDirectionSemaphores();
                DIRECTION oppositeDirection; // set to none
                if(args->DesiredDirection == RIGHT) oppositeDirection = LEFT;
                else oppositeDirection = RIGHT;
                // no need to unlock data. The reader threads in the (now reverse) direction have been killed by TERM
                toggleDirectionToAllSemaphores(sems,oppositeDirection);
                deleteDirectionSemaphores(sems);
            }
            // let nature take its course
            // the P-send thread of this process (not the one that sent TERM) will be killed by their parent processes, since they're blocked by scanf
            break;

        }
    }
    args->data = "TERM";
    writeToMemory(args);
    if(VERBOSITY)
        printf("%s Preceiver returning.\n",args->whoAmI);

}
// split msg & checksum to its parts
void splitAggregate(char *aggr,char * msg, char * md5)
{
    int i=0,j1=0,j2 = 0;
    int foundSeparator = 0;
    for(;i<strlen(aggr);++i)
    {
        if(aggr[i] == MESSAGE_CHSUM_SEPARATOR) { foundSeparator = 1; continue; }
        if(foundSeparator) md5[j2++] = aggr[i];
        else msg[j1++] = aggr[i];
    }
    msg[j1] = '\0';
    md5[j2] = '\0';

}

// check the checksum
int noErrorsInMessage(char * aggregate)
{
    if(VERBOSITY)
        printf("Checking message integrity...");
    char msg[MAX_MESSAGE_SIZE]; char chsum[MAX_ENCODING_SIZE];
    splitAggregate(aggregate,msg,chsum);
    //printf("aggregate was : %s\nmsg : %s , enc : %s \n",aggregate,msg,chsum);

    char * newchsum = getMD5(msg);
    //printf("New chsum computed %s\n",newchsum);

    int equal = strcmp(newchsum,chsum) == 0;
    free(newchsum);
    if(equal == 1) return  SUCCESS;
    return FAILURE;
}
void appendCheckSum(char ** dataptr)
{
    // ptr to ptr
    char * data = *dataptr;
    if(VERBOSITY)
        printf("Appending chsum to %s\n",data);

    int mlen = strlen(data);
    char * checksum = getMD5(data);
    int dataLen = strlen(data);
    int encLen = strlen(checksum);


    char * newData = (char*)malloc(2+dataLen + encLen * sizeof(char));

    memcpy(newData,data,dataLen);
    newData[dataLen] = '#';
    memcpy(&newData[dataLen+1],checksum,encLen);
    newData[dataLen+encLen+1] = '\0';

    free(data);
    free(checksum);
    *dataptr = newData;


}

void ENCencoder(continuousArgs *args)
{
    int doDie = 0;

    char storage[MAX_MESSAGE_SIZE + MAX_ENCODING_SIZE + 1];
    while(1)
    {
        int status = readFromMemory(args->read);
        if (status==0) continue;

        // check if the return from send was a direction change instead of a data read
        // if it is, continue the loop (do not write) and re-enter readFromMemory, where
        // you'll be blocked in the direction lock.
        if(currDirection(args->read) != args->read->DesiredDirection){printf("\t\t~%s Etransfer : not writing bcoz of dchange reader \n",args->read->whoAmI); continue; }

        if(strcmp(args->read->data,"TERM") == 0) doDie = 1;
        // keep data you just read in case you have to send them again
        // E : check if you have to
        // check for corruption or not
        if(!doDie)
        {
            if(strlen(args->read->data) == 0)
            {
                if(VERBOSITY)
                    printf("%s : enctransfer read zero-string read. Re-sending: %s\n",args->read->whoAmI,storage);
                strcpy(args->read->data,storage);
            }
            else
            {
                strcpy(storage,args->read->data);
            }
            appendCheckSum(&(args->read->data));

        }
        strcpy(args->write->data,args->read->data);

        writeToMemory(args->write);
        if(doDie) break;
    }
    if(VERBOSITY)
        printf("%s ENCencoder returning.\n",args->read->whoAmI);

}
void ENCdecoder(continuousArgs *args)
{
    int retryCount = 0;
    int doDie = 0;

    while(1)
    {
        int status = readFromMemory(args->read);
        if (status==0) continue;

        if(currDirection(args->read) != args->read->DesiredDirection) { continue; }

        if(strcmp(args->read->data,"TERM") == 0) doDie = 1;
        char msg[MAX_MESSAGE_SIZE];
        char enc[MAX_ENCODING_SIZE];

        if(!doDie)
        {
            // check message integrity
            if(noErrorsInMessage(args->read->data) != 1)
            {
                // if it's corrupted, request re-sending
                if(VERBOSITY)
                    printf("Message is corrupted\n");
                if(args->read->resendSem->id < 0)
                {
                    // we have not connected to it yet.
                    int status = connectToSemaphore(args->read->resendSem);
                    if(status == FAILURE)
                    {
                        printf("!!! %s : continuousECheck : Failed to connect to data sem : %d\n",args->read->whoAmI,args->read->resendSem->key);
                        pause();
                    }
                }
                // if we're ENC2, unlock the data semaphore in ENC1
                // to make ENC1 read the data again
                if(args->read->resendSem == NULL)
                {
                    printf("%s Echeck: error NULL resend semaphore!\n",args->read->whoAmI); pause();
                }
                unlock(args->read->resendSem);
                if(VERBOSITY)
                     printf("\t~**%s Echeck: requested re-sending.\n",args->read->whoAmI);
                ++retryCount;
                continue;
            }
            // else, pass it on
            if(retryCount > 0)
                printf("\t%d retries were needed.\n",retryCount);
            splitAggregate(args->read->data,msg,enc);
            strcpy(args->write->data,msg);
        }
        else
            strcpy(args->write->data,args->read->data);
        writeToMemory(args->write);
        if(doDie) break;
    }
    if(VERBOSITY)
        printf("%s ENCdecoder returning.\n",args->read->whoAmI);
}

// set noise to message
void corruptMessage(char * msg)
{

    if(strlen(msg) == 0)
    {
        printf("Zero size msg in corrupt()\n");
        return;
    }
    // find the length of the message-only
    int i=0;
    for(;i<strlen(msg);++i)
    {
        if(msg[i] == MESSAGE_CHSUM_SEPARATOR) break;
    }
    if(i==strlen(msg))
    {
        printf("ERROR : Did not find chsum separator\n");
        pause();
    }
    int msgLen = i;
    int prob = getRandomInteger(0,100);
    if(VERBOSITY)
        printf("Corrupt : randomed %d vs %d\n",prob,PROB_CORRUPTION);
    if(prob > PROB_CORRUPTION) return;
    if(VERBOSITY)
        printf("Corrupting message.\n");
    int pos = getRandomInteger(0,msgLen-1);
    char c = getRandomAlphanumeric();
    if(VERBOSITY)
        printf("Setting char @ position %d to %c\n",pos,c);
    msg[pos] = c;

}
// thread function for CHAN
void CHANthread(continuousArgs *args)
{
    int doDie = 0;
    while(1)
    {
        int status = readFromMemory(args->read);
        if(status == 0) continue;
        if(currDirection(args->read) != args->read->DesiredDirection) {printf("\t\t~%s Ctransfer : not writing bcoz of dchange reader \n",args->read->whoAmI); continue; }

        if(strcmp(args->read->data,"TERM") == 0)
        {
            printf("%s Ctransfer read TERM.",args->read->whoAmI);
            doDie = 1;
        }
        if(!doDie)
        {
            // check for corruption or not

            corruptMessage(args->read->data);

        }
        //args->write->data = ALLOCATE;
        strcpy(args->write->data,args->read->data);
        //free(args->read->data);
        if(VERBOSITY)
            printf("Ctransfer dir %d writes :[%s]\n",(int)args->read->DesiredDirection,args->write->data);
        writeToMemory(args->write);

        if(doDie) break;

    }
    printf("%s CHANthread returning.\n",args->read->whoAmI);

}
// for testing
void delayExit(char * procname)
{
    printf("done.");
    forcePrint();
    pause();
    int a = 5;
    int i=0;
    printf("Process %s exiting in ",procname);
    for(;i<a;++i)
    {
        printf("%d...",a-i);
        forcePrint();

        sleep(1);
    }
    printf("\n");
}



