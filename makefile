all: P E C
FLAGS=-lpthread -lm
P: p.c
	gcc -o p p.c md5.c $(FLAGS)
E: enc.c
	gcc -o e enc.c md5.c  $(FLAGS)
C: chan.c
	gcc -o c chan.c  md5.c  $(FLAGS)
clean:
	rm ./e ./p ./c
