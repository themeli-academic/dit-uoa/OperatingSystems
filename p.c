#include "settings.h"

// P1 or 2
char processName[10];

int main(int argc, char ** argv)

{


    semaphore semlock, semdata;
    memory mem, directionMem;
    semaphore *Left,*Right;

    //AllDirectionSemaphores * dirSems;

    directionMem.key = dirMem;
    // if the process is called as an entry point, there are no arguments.
    if (argc == 1)
    {
            sprintf(processName,"P1");
            // since a P-process will always initiate the transaction,
            // it creates the shared memory
            mem.key = (key_t) mP1E1;
            semlock.key = P1E1;
            semdata.key = dP1E1;

            //char keyString[MAX_KEY_STRSIZE];
            //sprintf(keyString,"%d",(int)key);
            //printf("%s made key %d , to string  : %s\n",processName,key,keyString);

            // create shared memory
            PRINT("Creating memory");
            CHECK(createSharedMemory(&mem),"Create shared memory.");

            // create direction shared memory
            PRINT("Creating  direction shared memory");
            CHECK(createDirectionMemory(&directionMem),"Create direction shared memory.");
            // write the initial direction in it
            *(directionMem.ptr) = 'R';
            *(directionMem.ptr + 1) = '\0';

            // since we bind that semaphore to that shared mem.
            // create semlock sem
            PRINT("Creating semlock");
            CHECK(initSemaphore(&semlock,1),"Init lcok semaphore.");
            // create semdata sem
            PRINT("Creating semdata");
            CHECK(initSemaphore(&semdata,0),"Init data semaphore.");

            // also create direction semaphores
            PRINT("Creating direction sem");
            Left = (semaphore*) malloc(sizeof(semaphore)); Left->key = P1L;
            CHECK(initSemaphore(Left,0),"Init left direction semaphore.");
            Right = (semaphore*) malloc(sizeof(semaphore)); Right->key = P1R;
            CHECK(initSemaphore(Right,1),"Init right direction semaphore.");


            // fork
            // ----------------

            pid_t pid = fork();
            if(pid < 0)
            {
                    printf("%s ERROR : enc fork failed!\n",processName);
                    return FAILURE;
            }
            if(pid == 0)
            {
                   // child process. P1 spawns e1
                    printf("%s is spawing an ENC1\n",processName);
                    // 2nd argument tells ENC what tp spawn

                    if(ENC_CHAN_SEPARATE_TERMINALS == 0)
                    {   execl("e","c",NULL);}
                    else
                    {
                        char command[100];
                        // dummy arg exists so that arg. count of P2 is > 1
                        // check p.c outer if: if argc==1, it's P1. Dumb ..
                        sprintf(command,"x-terminal-emulator -e \"./e c \"");
                        int returnValue = system(command);
                        if(returnValue < 0)
                        {
                            printf("Something went wrong while calling command:\n%s\n.Exiting.",command);
                            return FAILURE;
                        }
                        return SUCCESS;
                    }
            }
            else
            {
                if(ENC_CHAN_SEPARATE_TERMINALS == 1)
                {
                    PRINT("Parent waiting to get kid..");
                    // parent
                    if (waitpid(pid, NULL, 0) < 0)
                    {
                        perror("Failed to collect child process");
                        PRINT("KID FAILLLLL!!..");
                        return FAILURE;
                    }
                    else
                    {
                        printf("Consumed E1-creating child process successfully.\n");
                    }
                }
            }
            // ----------------


    } // if argc == 2, P2 is spawned as a system() call from ENC2
    else
    {
        sprintf(processName,"P2");

        // get the key value from the arguments
        // because P2 is called through system(), the first argument
        // is actually the executable name.
        // modify the argv,argc to skip the 1st arg
        argv++; argc--;
        mem.key = mE2P2;
        semlock.key = E2P2;
        semdata.key = dE2P2;
        CHECK(mem.key,"Shared memory key.");
        // connect to shared memory
        CHECK(connectToSharedMemory(&mem),"Connect to shared memory.");
        // connect to direction memory
        CHECK(connectToDirectionMemory(&directionMem),"Connect to direction shared memory.");
        // connect to semaphores
        PRINT("Connecting to left lock and data");
        CHECK(connectToSemaphore(&semlock),"Connect to left lock");
        CHECK(connectToSemaphore(&semdata),"Connect to left data");
        // also create direction semaphores
        PRINT("Creating direction sem");
        Left = (semaphore*) malloc(sizeof(semaphore)); Left->key = P2L;
        CHECK(initSemaphore(Left,0),"Init left direction semaphore.");
        Right = (semaphore*) malloc(sizeof(semaphore)); Right->key = P2R;
        CHECK(initSemaphore(Right,1),"Init right direction semaphore.");
    }

    // creation done above.
    // below is behaviour
    // false below. P des not act like that
    // no readwrite
    pthread_t threadUser, threadEndpoint;


    PRINT("***********************\n**********************\nCreating threads");
    if(strcmp(processName,"P1") == 0)
    {
        char msgS[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        char msgR[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];

        threadArgs argsSend = makeThreadArgs(processName,msgS,&mem,&semlock,&semdata,Right,&directionMem);
        threadArgs argsReceive = makeThreadArgs(processName,msgR,&mem,&semlock,&semdata,Left,&directionMem);

        argsSend.direction = Right; // the semaphore
        argsSend.DesiredDirection = RIGHT; // a value for comparing

        // in P1, getting user message works only in RIGHT
//        printf("%s passing to contPSend desired : %d\n",processName,args.DesiredDirection);
        CHECK(pthread_create(&threadUser,NULL,(void*)&Psender,&argsSend),"Failed to create user input thead");
        // in P1, getting user message works only in LEFT
        argsReceive.direction = Left;
        argsReceive.DesiredDirection = LEFT;
        CHECK(pthread_create(&threadEndpoint,NULL,(void*)&Preceiver,&argsReceive),"Failed to create reception thead");

    }
    else
    {
        char msgS[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        char msgR[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
        // P2
        // P2 sends to the left
        threadArgs argsSend = makeThreadArgs(processName,msgS,&mem,&semlock,&semdata,Left,&directionMem);
        argsSend.DesiredDirection = LEFT;
        // receives from left to right
        threadArgs argsReceive = makeThreadArgs(processName,msgR,&mem,&semlock,&semdata,Right,&directionMem);
        argsReceive.DesiredDirection = RIGHT;

        CHECK(pthread_create(&threadUser,NULL,(void*)&Psender,&argsSend),"Failed to create user input thead");
        CHECK(pthread_create(&threadEndpoint,NULL,(void*)&Preceiver,&argsReceive),"Failed to create reception thead");
    }

    pthread_join(threadEndpoint, NULL);
    PRINT("Endpoint thread joined.");
    pthread_cancel(threadUser);
    PRINT("Sender thread cancelled.");
    pthread_join(threadUser, NULL);
    PRINT("Sender thread joined.");


    // clean up
    // delete memories and semaphores
    shmctl(mem.id, IPC_RMID, 0);
    shmctl(directionMem.id, IPC_RMID, 0);

    semctl (semlock.id,0,IPC_RMID,0);
    semctl (semdata.id,0,IPC_RMID,0);

    semctl (Left->id,0,IPC_RMID,0);
    semctl (Right->id,0,IPC_RMID,0);
    free(Left); free(Right);

    if(PAUSE_BEFORE_EXIT) delayExit(processName);
    return SUCCESS;
}


