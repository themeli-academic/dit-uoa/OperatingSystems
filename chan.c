#include "settings.h"



char processName[10] = "CHAN";

int main(int argc, char ** argv)
{

    initRandomize();

    semaphore dataLeft,dataRight,lockLeft,lockRight;
    memory memLeft,memRight, directionMem;

    semaphore *Left,*Right;
    memLeft.key = mE1C;
    dataLeft.key = dE1C;
    lockLeft.key = E1C;

    memRight.key = mCE2;
    dataRight.key = dCE2;
    lockRight.key = CE2;
    directionMem.key = dirMem;

    //  connect to shared memory
    PRINT("Connecting to left shared memory");
    CHECK(connectToSharedMemory (&memLeft),"Connect to left shared memory");
    CHECK(createSharedMemory (&memRight),"Create right shared memory");
    CHECK(connectToDirectionMemory (&directionMem),"Connect to left shared memory");
    // create and connect to semaphore
    // connect
    PRINT("Connecting to left lock and data");
    CHECK(connectToSemaphore(&lockLeft),"Connect to left lock");
    CHECK(connectToSemaphore(&dataLeft),"Connect to left data");
    //create
    PRINT("Creating right lock and data");
    CHECK(initSemaphore(&lockRight,1),"Connect to right data");
    CHECK(initSemaphore(&dataRight,0),"Connect to right data");

    // also create direction semaphores
    PRINT("Creating direction sem");
    Left = (semaphore*) malloc(sizeof(semaphore)); Left->key = CL;
    CHECK(initSemaphore(Left,0),"Init left direction semaphore.");
    Right = (semaphore*) malloc(sizeof(semaphore)); Right->key = CR;
    CHECK(initSemaphore(Right,1),"Init right direction semaphore.");

    // create the right shared memory
    PRINT("Creating right memory");
    CHECK(createSharedMemory(&memRight),"Right shared memory");




    // create the other ENC
    pid_t pid = fork();
    CHECK(pid,"Fork failed.");
    if(pid == 0)
    {
           // child process
            printf("%s-child is spawing ENC\n",processName);
            if(ENC_CHAN_SEPARATE_TERMINALS == 0)
            {
                execl("e","p", NULL);
            }
            else
            {
                char command[100];
                // dummy arg exists so that arg. count of P2 is > 1
                // check p.c outer if: if argc==1, it's P1. Dumb ..
                sprintf(command,"x-terminal-emulator -e \"./e p \"");
                int returnValue = system(command);
                if(returnValue < 0)
                {
                    printf("Something went wrong while calling command:\n%s\n.Exiting.",command);
                    return FAILURE;
                }
                return SUCCESS;
            }
    }
    else
    {
        if(ENC_CHAN_SEPARATE_TERMINALS == 1)
        {
            if (waitpid(pid, NULL, 0) < 0)
            {
                perror("Failed to collect child process");
                return FAILURE;
            }
            else
            {
                printf("Consumed C-creating child process successfully.\n");
            }
        }
    }

    // end of creation
    // start of behaviour

    pthread_t thread1,thread2;
    char msgR[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];
    char msgL[MAX_MESSAGE_SIZE+MAX_ENCODING_SIZE+1];

    continuousArgs argsR;
    threadArgs readR = makeThreadArgs(processName,msgR,&memLeft,&lockLeft,&dataLeft,Right,&directionMem);
    threadArgs writeR = makeThreadArgs(processName,msgR,&memRight,&lockRight,&dataRight,Right,&directionMem);
    readR.DesiredDirection = RIGHT;
    writeR.DesiredDirection = RIGHT;


    argsR.read = &readR; argsR.write = &writeR;

    CHECK(pthread_create(&thread1,NULL,(void*)&CHANthread,&argsR),"Failed to create chan right thead");

    continuousArgs argsL;
    threadArgs readL = makeThreadArgs(processName,msgL,&memRight,&lockRight,&dataRight,Left,&directionMem);
    threadArgs writeL = makeThreadArgs(processName,msgL,&memLeft,&lockLeft,&dataLeft,Left,&directionMem);
    readL.DesiredDirection = LEFT;
    writeL.DesiredDirection = LEFT;
    argsL.read = &readL; argsL.write = &writeL;

    CHECK(pthread_create(&thread2,NULL,(void*)&CHANthread,&argsL),"Failed to create chan left thead");


    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    // delete the memories not accessible by the P processes
    shmctl(memLeft.id, IPC_RMID, 0);
    shmctl(memRight.id, IPC_RMID, 0);
    // and their semaphores
    semctl (lockRight.id,0,IPC_RMID,0);
    semctl (dataRight.id,0,IPC_RMID,0);
    semctl (lockLeft.id,0,IPC_RMID,0);
    semctl (dataLeft.id,0,IPC_RMID,0);

    semctl (Left->id,0,IPC_RMID,0);
    semctl (Right->id,0,IPC_RMID,0);
    free(Left); free(Right);

    if(PAUSE_BEFORE_EXIT) delayExit(processName);
    return SUCCESS;
}
